#!/bin/bash

# Echo a message to the terminal
# @default {string} default - If no message is passed it will default to this variable
# @param {string} message - the message that you want echoed
# @example
#   ./message.sh yeet

# PARAMS
default="Process is running"
message="${1:-$default}"

echo "[$(date -Iseconds)] $message"
