#!/bin/bash

# borgbackup script to backup directories
# @param {string} borg_repo - path to the borg repo
# @param {array} directories_to_backup - list of all the directories to backup
# @param {string} password - password for borg repo
# @example
#   ./borg-backup-directories.sh /home/usr/borg-repo ("/media/sda1/dir1" "/media/sda1/dir2") foo

# PARAMS
borg_repo=$1
directories_to_backup=$2
password=$3

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"

$message "syncing backups to borg"

export BORG_PASSPHRASE=$password

for i in $directories_to_backup; do
  # On every loop we redefine this variable to get a new name
  archive_name="$(hostname)-$(date -Iseconds | cut -d '+' -f 1)"

  borg create --progress --stats --compression zlib "$borg_repo::$archive_name" $i
done

$message "borg backups have finished syncing"
