#!/bin/bash

# borgbackup installation and setup
# @param {string} borg_repo - path to the borg repo
# @example
#   ./setup_borg.sh /home/usr/borg-repo

# PARAMS
borg_repo=$1

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
package_installer="$location_dir/package-installer.sh"
package_manager=$("$location_dir/get-system-package-manager.sh")

# Setup

if [ -z "$borg_repo" ]
  then
    $message "Borg needs a path to an empty repository to create the backups. \
    For more information: https://borgbackup.readthedocs.io/en/stable/usage/init.html"
    exit 1
fi

if [ "$(ls -A "$borg_repo")" ]
then
    $message "$borg_repo exists in your filesystem and it is not empty."
    exit 1
fi

if [$package_manager == "pacman" ]
then
  $package_installer "borg"
else
  $package_installer "borgbackup"
fi

if [ ! -d "$borg_repo" ]
then
  $message "Creating $borg_repo directory in your filesystem"
  sudo mkdir $borg_repo
fi

borg init --encryption=repokey-blake2 $borg_repo
borg key export $borg_repo ~/borg-key-backup.txt
$message "borg repo created and your backup key is located on ~/borg-key-backup.txt"
